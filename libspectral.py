#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np 
from scipy import signal as sig
from scipy.signal import periodogram    


# ============
#
# 
#
# ============
def psd_analytical(L,dx,intercept,slope):
    '''
    '''
    
    longueur_signal  = L
    x  = np.arange(0.0,longueur_signal+dx,dx)
    Nech_signal = x.size
    
    fbin = 1./(dx*Nech_signal)
    fmax = 1./(2*dx)
    fmin = 1./longueur_signal

    # # aR^b
    a = intercept
    b = slope
    
    f   = np.arange(fmin,fmax+fbin,fbin)
    Pxx = 10**(b*np.log10(f) + a)

    print('\t--------------------- Signal')                
    print('\t\t dx   = %.2f [km]'%dx)
    print('\t\t L    = %.2f [km]'%longueur_signal)
    print('\t\t Nech = %.2f'%Nech_signal)
    print('\t--------------------- Spectrum')                
    print('\t\t fmin  = %.2f'%fmin)
    print('\t\t fmax  = %.2f'%fmax)
    print('\t\t fbin  = %g'%fbin)
    print('\t\t Nfreq = %d'%Pxx.size)
    print('\n')
    

    return f,Pxx

# ============
#
# 
#
# ============
def psd_wtc_analytical(L,dx):
    '''
    src: Swot mission performance and error budget, 7th April 2017, JPL D-79084
    p 35-36 sur 115

    To derive the overall error
    spectrum, we consider a 2D slope of p=8/3+1, which is consistent with all available observations
    from AMRS-E and the Jason-1/2 AMRs for wavelengths > 100 km. This results in a 1D wet
    tropo signal given by [...]
    At scales shorter than 100 km, high-resolution measurements made by JPL’s High Altitude
    MMIC Sounding Radiometer (HAMSR), indicate a slightly lower slope of -2.33, for a wet tropo signal given by [...]
    '''

    longueur_signal  = L
    x  = np.arange(0.0,longueur_signal+dx,dx)
    Nech_signal = x.size
    
    fbin = 1./(dx*Nech_signal)
    fmax = 1./(2*dx)
    fmin = 1./longueur_signal

    f   = np.arange(fmin,fmax+fbin,fbin)

    Pxx = np.zeros(f.size)

    indok = np.where(f<1e-2)
    Pxx[indok] = 3.156e-5*f[indok]**(-8/3)
    
    indok = np.where(1e-2<=f)
    Pxx[indok] = 1.4875e-4*f[indok]**(-2.33)

    print('\t--------------------- Signal')                
    print('\t\t dx   = %.2f [km]'%dx)
    print('\t\t L    = %.2f [km]'%longueur_signal)
    print('\t\t Nech = %.2f'%Nech_signal)
    print('\t--------------------- Spectrum')                
    print('\t\t fmin  = %.2f'%fmin)
    print('\t\t fmax  = %.2f'%fmax)
    print('\t\t fbin  = %g'%fbin)
    print('\t\t Nfreq = %d'%Pxx.size)
    print('\n')
    

    return f,Pxx

# ============
#
# 
#
# ============
def psd_to_signal(Pxx,L_signal,dx,window):
        '''
        '''
        
        print('======================= From spectrum to signal')    
        x_signal  = np.arange(0.0,L_signal+dx,dx)
        Nech_signal = x_signal.size
        # twice the spatial sampling
        # ~ fbin = 1./(dx*Nech_signal)
        fbin = 2*1./(dx*Nech_signal)


        Nfreq    = Pxx.size
        # ~ normIFFT = Nfreq*np.sqrt(2*fbin)
        normIFFT = Nfreq*np.sqrt(fbin)/2
        

        
        signal = np.empty(0)
        w_pxx_ana  = sig.get_window(window, Nfreq)
        ampl       = np.sqrt(Pxx*fbin*((w_pxx_ana*w_pxx_ana).sum()))       
        Nb_segments = int(np.ceil(Nech_signal/Nfreq))
        for k in range(Nb_segments):
            phase      = 2*np.pi*np.random.randn(Nfreq)
            signal = np.hstack((signal,np.real(np.fft.ifft(ampl*np.exp(1j*phase))*normIFFT)))

        print('\t\tlength signal (rec)  = %.2f km'%L_signal)
        print('\t\tNech (rec)   = %d'%Nech_signal)
        print('\t\tdx   (rec)   = %g'%dx)
        print('\t\t1/dx (rec)   = %g'%(1./dx))
        print('\t\tdf = 1/(dx*Nech) (rec)  = %g\n\n'%(1/(dx*Nech_signal)))

        return x_signal,signal

# ============
#
# 
#
# ============
def signal_to_segments(signal,L_signal,L_segment,dx):
    '''
    '''
    

    print('======================= Split signal into segments')                

    x_signal        = np.arange(0.0,L_signal+dx,dx)    
    Nech_signal     = x_signal.size

    x_segment        = np.arange(0.0,L_segment+dx,dx)    
    Nech_segment     = x_segment.size


    Nb_segments      = np.int(Nech_signal/Nech_segment)

    segments = np.reshape(signal[0:Nech_segment*Nb_segments],(Nb_segments,Nech_segment))

    print('\t--------------------- Segments')                
    print('\t\t%d segments of %d samples and a length of %f km'%(Nb_segments,Nech_segment,L_segment))
    print('\t\tsegments.shape = %d,%d\n'%(segments.shape[0],segments.shape[1]))
    
    return segments
    
# ============
#
# 
#
# ============
def periodogram_median(Tab_EchantillonTemp, fs=0.17, win=None, reg='linear', dsp='density'):
    '''
    '''

    N = np.size(Tab_EchantillonTemp,0)
    
    # Calcul N spectres:
    # ------------------------
    firstsample = False
    nbok = 0
    nbperiodo = 0
    for j in range(N):
        if np.sum(np.isnan(Tab_EchantillonTemp[j])) == 0:
            freq, Spectre = periodogram(Tab_EchantillonTemp[j], fs, window=win, detrend=reg, scaling=dsp)
            
            #Spectre = Spectre*(np.pi/2)
            
            
            if not firstsample:
                freq_Tab = freq[1:]
                Sp_Tab = Spectre[1:]
                firstsample = True
            else:
              try:
                if (np.sum(np.isnan(freq[1:])) == 0):
                    freq_Tab=np.vstack((freq_Tab,freq[1:]))
                    Sp_Tab=np.vstack((Sp_Tab,Spectre[1:]))
                    nbperiodo +=1
              except:
                if (np.sum(np.isnan(freq[1:])) == 0):
                    freq_Tab = freq_Tab
                    Sp_Tab = Sp_Tab
                    
            nbok +=1
                    
        else:
            pass
            # ~ print 'Nan dans l echantillon %s' % j 

    # Calcul du spectre moyen:
    # ------------------------
    if N==1:
        freq_Moy = freq_Tab
        Sp_Moy = Sp_Tab
        #print '1 seul periodogramme: pas de moyenne' 
    else:
        freq_Moy = np.median(freq_Tab,axis=0)
        Sp_Moy   = np.median(Sp_Tab,axis=0)


    #print 'fin periodogramme moyen'
    return freq_Moy, Sp_Moy, freq_Tab, Sp_Tab, win

# ============
#
# 
#
# ============
def get_antenna(diameter_fov,L_antenna,dx,type='gauss'):
    '''
    '''
    
    x_antenna  = np.arange(0.0,L_antenna,dx) - np.floor(L_antenna/2)
    
    if type =='gauss':
        Sigma   = diameter_fov / np.sqrt(8*np.log(2));
        antenna = (1.0 / np.sqrt(2*np.pi*Sigma**2)) * np.exp(-0.5*x_antenna**2/Sigma**2)


    # normalisation
    antenna = antenna/antenna.sum()


    return x_antenna,antenna


# ============
#
# 
#
# ============
def convolution(x_signal,signal,dx,antenna,dx_antenna):
    '''
    '''
    
    
    signal_convol = np.convolve(signal,antenna,'same')    
    
    # il faut tenir compte du sous-échantillonnage dû à la simulation du signal à partir du spectre
    dn = np.int(dx_antenna/dx)
    if dn==0: dn = 1 
    x_signal        =        x_signal[::dn]
    signal_convol   =   signal_convol[::dn]

    dx_convol    = dx_antenna
    Nech_convol  = signal_convol.size
    L_convol     = x_signal.max()

    print('\t--------------------- Signal after convolution')                
    print('\t\tlength   = %.1f km'%L_convol)
    print('\t\tNech (direct)  = %d'%Nech_convol)
    print('\t\tdx   (direct) = %g'%dx_convol)

    return x_signal,signal_convol,dx_convol,Nech_convol,L_convol
    
# ============
#
# 
#
# ============
def get_parseval(f,Px,lim=None):
    '''
    lim = [km]
    '''
    
    if lim is not None:
        indok = np.where(1.0/f<=lim)
        f  = f[indok]
        Px = Px[indok]
    
    return np.sqrt(np.trapz(Px,f))

    