# Spectral Sandbox

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/bpicard_fluctus%2Fspectral-sandbox/master) 


## Sandbox for spectral analysis

* Generate a PSD from an analytical formula (a * f**b)

* Generate a signal in the direct domain from the PSD (random trial of the phase)

* Apply spatial convolution to simulate the impact of an antenna pattern

* Apply gaussian noise to simulate instrumental/retrieval error/noise

* Compute median PSD from direct signal splitted in segments

* Compare to the source PSD